# Teoschool Project App : Frontend

## Frontend Docker image for the Spring Rest project App

---
To setup and run this project, it is necessary to run the backend part over here : https://gitlab.com/dart0/teoschool-project-app-back

Service is exposed on port 4200 by default.

---
Building and running the image
---

Build with `docker build --rm -t your-image-name-here:tag DOCKERFILE_LOCATION`

Run with `docker run -d -p HOST_PORT:DEFAULT_PORT your-image-name-here:tag`