FROM node:14.4.0 AS builder
RUN npm uninstall -g angular-cli @angular/cli
RUN npm cache clean -f
COPY . /app
WORKDIR /app
RUN git clone https://github.com/spring-petclinic/spring-petclinic-angular.git
WORKDIR /app/spring-petclinic-angular
RUN npm install -g @angular/cli@8.0.3
RUN npm install --save-dev @angular/cli@8.0.3
RUN rm package-lock.json
RUN ng version
RUN npm version
RUN npm install
RUN ng build --prod --base-href=/petclinic/ --deploy-url=/petclinic/

FROM nginx:1.18-alpine
COPY nginx.conf /etc/nginx/nginx.conf
WORKDIR /usr/share/nginx/html/petclinic
COPY --from=builder /app/spring-petclinic-angular/dist/ .
ADD docker-entrypoint.sh .
RUN ["chmod", "+x", "./docker-entrypoint.sh"]
RUN ./docker-entrypoint.sh